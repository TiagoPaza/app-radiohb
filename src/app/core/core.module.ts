import { NgModule } from '@angular/core';
import { NativeScriptModule } from 'nativescript-angular/nativescript.module';

import { NewService } from './new.service';
import { PlayerStatusService } from './player-status.service';
import { CommunityService } from './community.service';
import { ChargeService } from './charge.service';
@NgModule({
    imports: [
        NativeScriptModule,
    ],
    declarations: [],
    providers: [
        ChargeService,
        CommunityService,
        NewService,
        PlayerStatusService
    ],
})
export class CoreModule { }