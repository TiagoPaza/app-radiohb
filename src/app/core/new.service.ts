import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()

export class NewService {
	constructor(private http: HttpClient) {
	}

	public API_URL = "http://radiohb.paza.net.br/api";

	private extractData(res: Response) {
		let body = res;
		return body || {};
	}

	apiHeaders() {
		let headers = new HttpHeaders({
			"Content-Type": "application/json",
			"Accept": "application/json"
		});

		return headers;
	}

	getAll(page: number): Observable<any> {
		return this.http.get(this.API_URL + `/news?limit=12&page=${page}`).pipe(map(this.extractData));
	}

	get(id: number): Observable<any> {
		return this.http.get(this.API_URL + `/news/${id}`).pipe(map(this.extractData));
	}

	find(request: string): Observable<any> {
		return this.http.get(this.API_URL + "/news/search/?find=" + request).pipe(map(this.extractData));
	}
}