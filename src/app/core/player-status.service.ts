import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";

@Injectable()

export class PlayerStatusService {
	public API_URL = "https://radiohabblet.com";

	constructor(public http: HttpClient) {
	}

	private extractData(res: Response) {
		let body = res;
		return body || { };
	}
	
	apiHeaders () {
		let headers = new HttpHeaders({
			"Content-Type": "application/json",
			"Accept": "application/json"
		});
		
		return headers;
	}

	get(): Observable<any> {
		return this.http.get(this.API_URL + "/a/Update.json").pipe(map(this.extractData));
	}
}