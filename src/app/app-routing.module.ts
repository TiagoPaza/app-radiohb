import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", loadChildren: "~/app/home/home.module#HomeModule" },
    { path: "radio", loadChildren: "~/app/radio/radio.module#RadioModule" },
    { path: "search", loadChildren: "~/app/search/search.module#SearchModule" },
    { path: "new-detail/:index", loadChildren: "~/app/new-detail/new-detail.module#NoticeDetailModule" },
    { path: "settings", loadChildren: "~/app/settings/settings.module#SettingsModule" }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
