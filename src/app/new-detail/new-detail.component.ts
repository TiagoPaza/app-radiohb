import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute } from "@angular/router";
import { NewService } from "../core";
import { WebView } from "tns-core-modules/ui/web-view";

@Component({
    selector: "new-detail",
    templateUrl: "./new-detail.component.html",
    styleUrls: ["./new-detail.component.css"],
    moduleId: module.id
})
export class NewDetailComponent implements OnInit {
    newDetail: number;
    infoNew: any;

    constructor(private routerExtensions: RouterExtensions, private route: ActivatedRoute, private newService: NewService) {
    }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.newDetail = params['index'];
            this.newService.get(this.newDetail).subscribe((response) => {
                this.infoNew = response.data.attributes;
            })
        });
    }

    getBack() {
        this.routerExtensions.back();
    }
}
