import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { NoticeDetailRoutingModule } from "./new-detail.routing.module";
import { NewDetailComponent } from "./new-detail.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NoticeDetailRoutingModule
    ],
    declarations: [
        NewDetailComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class NoticeDetailModule { }
