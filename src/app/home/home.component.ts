import * as app from "tns-core-modules/application";
import { Component, OnInit, ViewChild } from "@angular/core";
import { NewService, CommunityService } from "../core";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { RadListView, LoadOnDemandListViewEventData } from "nativescript-ui-listview";
import { PlayerStatusService } from "../core/player-status.service";
import { RadListViewComponent } from "nativescript-ui-listview/angular/listview-directives";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
    public news: any;
    public communities: any;
    public numPage: number;
    public item: string;
    public status: any;
    public pagination: any;
    private _player: any;
    @ViewChild("#newsListView") listViewComponent: RadListViewComponent;
    // @ViewChild("#communitiesListView") listViewComponent: RadListViewComponent;

    constructor(page: Page, private routerExtensions: RouterExtensions, private newService: NewService, private playerStatusService: PlayerStatusService,
        private communityService: CommunityService) {
       this._player =  <any>page.getViewById("ExoPlayer");
    }

    ngOnInit(): void {
        this.news = [];
        this.communities = [];
        this.status = [];

        this.numPage = 1;
        this.showNews(this.numPage);
        this.showStatus();
        this.showCommunities(this.numPage);

        setInterval(() => {
            this.showStatus();
        }, 5000);
    }

    togglePlayer(type: string) {
        if (type == "PAUSE") { 
            this._player.pause();
        } else {
            this._player.play();
        }
    }

    moreItems(args: LoadOnDemandListViewEventData) {
        const that = new WeakRef(this);
        const listView: RadListView = args.object;

        if (this.numPage < this.pagination.total_pages) {
            this.numPage = this.numPage + 1;
            
            this.showNews(this.numPage);
            this.showCommunities(this.numPage);

            listView.notifyLoadOnDemandFinished();
            args.returnValue = true;
        } else {
            args.returnValue = false;
            listView.notifyLoadOnDemandFinished(true);
        }
    }

    showStatus() {
        this.playerStatusService.get().subscribe((response) => {
            this.status = response.radio;
        });
    }
    
    showNews(numPage: number) {
        this.newService.getAll(numPage).subscribe((response) => {
            this.news.push(...response.data);
            this.pagination = response.meta.pagination;
        });
    }

    showNew(args) {
        let id = args.view.bindingContext.id;
        this.routerExtensions.navigate(["new-detail", id]);
    }

    showCommunities(numPage: number) {
        this.communityService.getAll(numPage).subscribe((response) => {
            this.communities.push(...response.data);
            this.pagination = response.meta.pagination;
        });
    }

    showCommunity(args) {
        let id = args.view.bindingContext.id;
        this.routerExtensions.navigate(["community-detail", id]);
    }

    goMenu(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
