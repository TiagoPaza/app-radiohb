import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { RadioRoutingModule } from "./radio-routing.module";
import { RadioComponent } from "./radio.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        RadioRoutingModule
    ],
    declarations: [
        RadioComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class RadioModule { }
